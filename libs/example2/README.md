# example2

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build example2` to build the library.

## Running unit tests

Run `nx test example2` to execute the unit tests via [Jest](https://jestjs.io).
