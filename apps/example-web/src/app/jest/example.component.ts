import {
  HttpClient,
  HttpEventType,
  HttpRequest,
  HttpResponse,
} from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  inject,
} from '@angular/core';
import { take, filter, map, of, delay, tap } from 'rxjs';
import { ExampleService } from './example.service';

@Component({
  standalone: true,
  imports: [],
  template: ``,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExampleComponent implements OnInit {
  public num = 123;
  public obj = {
    ab: 'cd',
    z: undefined,
  };
  public error = new Error();

  public _stuff = 0;

  public ngOnInit(): void {
    this._stuff = 233;
  }

  public get stuff() {
    return this._stuff;
  }

  // http testing
  private readonly http = inject(HttpClient);

  public call1() {
    this.http
      .post('https://give.me/my/data', {
        time: '123',
      })
      .pipe(take(1))
      .subscribe((data) => {
        console.log(data);
      });
  }

  public call2() {
    const request = new HttpRequest('POST', 'https://give.me/my/data', {
      time: '123',
    });

    this.http
      .request(request)
      .pipe(
        filter(
          (response): response is HttpResponse<unknown> =>
            response.type === HttpEventType.Response
        ),
        map((response) => response.body),
        take(1)
      )
      .subscribe((data) => {
        console.log(data);
      });
  }

  // timings
  public callTimings() {
    of(0).pipe(delay(200), tap(console.log)).subscribe();
  }

  public dateExample(): string {
    return new Date().toISOString();
  }

  private readonly exampleService = inject(ExampleService);
  public logExample() {
    console.log(this.exampleService.serviceFunction());
  }
}
