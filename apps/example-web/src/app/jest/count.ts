export function count(arr: unknown[]): number {
  if (arr.length === 0) {
    return -1;
  }
  return arr.length;
}
