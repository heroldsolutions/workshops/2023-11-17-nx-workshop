import {
  ComponentFixture,
  TestBed,
  fakeAsync,
  tick,
} from '@angular/core/testing';
import { ExampleComponent } from './example.component';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { ExampleService } from './example.service';

describe('ExampleComponent', () => {
  let component: ExampleComponent;
  let fixture: ComponentFixture<ExampleComponent>;

  let http: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ExampleComponent, HttpClientTestingModule],
    }).compileComponents();
    fixture = TestBed.createComponent(ExampleComponent);
    component = fixture.componentInstance;

    http = TestBed.inject(HttpTestingController);
  });

  it('constructor()', () => {
    expect(component).toBeTruthy();
    expect(component.num).toBe(123);
    expect(component.obj).toEqual({
      ab: 'cd',
    });
    expect(component.obj).toStrictEqual({
      ab: 'cd',
      z: undefined,
    });
    expect(component.error instanceof Error).toBe(true);
    expect(component.error).toBeInstanceOf(Error);
    expect(component._stuff).toBe(0);
  });

  it('ngOnInit()', () => {
    component.ngOnInit();
    // fixture.detectChanges();

    expect(component.stuff).toBe(233);
  });

  it('call1()', () => {
    const spyLog = jest.spyOn(console, 'log').mockImplementation(() => {});

    // component.call1();
    component.call2();

    const request = http.expectOne('https://give.me/my/data');
    expect(request.request.method).toBe('POST');
    expect(request.request.body).toStrictEqual({
      time: '123',
    });

    request.flush({
      data: 'flushing',
    });

    expect(spyLog).toHaveBeenCalled();
    expect(spyLog).toHaveBeenCalledTimes(1);
    expect(spyLog).toHaveBeenCalledWith({
      data: 'flushing',
    });
    expect(spyLog).toHaveBeenNthCalledWith(1, {
      data: 'flushing',
    });
  });

  it('callTimings()', fakeAsync(() => {
    const spyLog = jest.spyOn(console, 'log').mockImplementation(() => {});

    component.callTimings();

    tick(200);

    expect(spyLog).toHaveBeenCalledTimes(1);
  }));

  it('dateExample()', () => {
    jest.useFakeTimers();
    jest.setSystemTime(new Date(2022, 1, 1, 0, 0, 0, 0));
    expect(component.dateExample()).toBe('2022-01-31T23:00:00.000Z');
    jest.useRealTimers();
  });

  describe('logExample()', () => {
    it('should be any number', () => {
      const spyLog = jest.spyOn(console, 'log').mockImplementation(() => {});

      component.logExample();

      expect(spyLog).toHaveBeenCalledWith(expect.any(Number));
    });

    it('should not execute the real function', () => {
      const service = TestBed.inject(ExampleService);
      jest.spyOn(service, 'serviceFunction').mockImplementation(() => 1);
      const spyLog = jest.spyOn(console, 'log').mockImplementation(() => {});

      component.logExample();

      expect(spyLog).toHaveBeenCalledWith(1);
    });
  });
});

class FakeExampleService implements ExampleService {
  public serviceFunction = (): number => 1;
}

describe('ExampleComponent2', () => {
  let component: ExampleComponent;
  let fixture: ComponentFixture<ExampleComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ExampleComponent, HttpClientTestingModule],
      providers: [
        {
          provide: ExampleService,
          useClass: FakeExampleService,
        },
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(ExampleComponent);
    component = fixture.componentInstance;
  });

  it('logExample()', () => {
    // const spyLog = jest.spyOn(console, 'log').mockImplementation(() => {});
    console.log = jest.fn();

    component.logExample();

    expect(console.log).toHaveBeenCalledWith(1);
  });
});
