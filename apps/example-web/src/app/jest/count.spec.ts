import { count } from './count';

describe('count()', () => {
  beforeAll(() => {
    // runs on describe init
  });

  beforeEach(() => {
    // runs before every test case
  });

  afterEach(() => {
    // runs after each test
  });

  afterAll(() => {
    // after all test cases are finished
  });

  it('should count an array', () => {
    const data = ['1', '2', '3'];
    const length = count(data);
    expect(length).toBe(3);
  });

  it('should return -1 on empty array', () => {
    expect(count([])).toBe(-1);
  });

  describe('', () => {});
});
