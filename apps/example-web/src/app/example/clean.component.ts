import { ChangeDetectionStrategy, Component } from '@angular/core';
import HiComponent from './hi.component';
import { BarPipe } from './bar.pipe';
import { FooDirective } from './foo.directive';
import { debounceTime, map, of, timer } from 'rxjs';
import { AsyncPipe } from '@angular/common';

@Component({
  selector: 'example-clean',
  standalone: true,
  imports: [HiComponent, BarPipe, FooDirective, AsyncPipe],
  template: `
    <button (click)="isVisible = true">click</button>
    <!-- @defer (on hover(hoverArea)) { -->
    <!-- @defer (when isVisible ) { -->
    @defer (when (error$ | async) ) {
    <example-hi exampleFoo />
    {{ 'foo' | bar }}
    } @loading { loading...} @error{ failed...} @placeholder {<span
      >placeholder </span
    >}

    <div #hoverArea>hover here</div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export default class CleanComponent {
  public isVisible = false;

  public error$ = of(0).pipe(
    debounceTime(2000),
    map(() => {
      // throw new Error();
      return true;
    })
  );
}
