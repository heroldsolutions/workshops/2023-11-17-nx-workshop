import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'example-defer',
  standalone: true,
  imports: [CommonModule],
  template: `defered component`,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export default class DeferComponent {}
