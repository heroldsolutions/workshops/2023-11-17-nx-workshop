import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'example-addition',
  standalone: true,
  imports: [CommonModule],
  template: `some text`,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export default class AdditionComponent {}
