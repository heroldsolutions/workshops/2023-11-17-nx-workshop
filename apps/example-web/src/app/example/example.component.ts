import { CommonModule, NgIf } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import AdditionComponent from './addition.component';
import { MatDividerModule } from '@angular/material/divider';
import DeferComponent from './defer.component';

@Component({
  standalone: true,
  imports: [
    NgIf,
    MatButtonModule,
    AdditionComponent,
    MatDividerModule,
    DeferComponent,
  ],
  template: `
    <div>
      example page
      <!-- if else -->
      <button mat-raised-button (click)="bool = !bool">my text</button>
      @if (bool) {
      <div>
        <example-addition />
      </div>
      } @else { normal else }
      <mat-divider />
      <!-- for loop -->
      <button mat-raised-button (click)="addListData()">my text</button>
      @for (data of arrayData; track data.id) {
      {{ data.description }}
      } @empty { Empty list }
      <mat-divider />
      <!-- switch -->
      <button mat-raised-button (click)="randomLevel()">my text</button>
      @switch (level) { @case ("1") { Level 1 Level 1 Level 1 Level 1 Level 1
      Level 1 Level 1 } @case ("2") { Level 2 } @case ("3") { Level 3 } @default
      { No Level} }

      <!-- defer -->
      <mat-divider />
      <div style="height: 50vh"></div>
      @defer (when bool) {
      <example-defer />
      }
    </div>
  `,
  styles: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export default class ExampleComponent {
  public bool = false;

  public arrayData: {
    id: number;
    description: string;
  }[] = [];

  public addListData(): void {
    this.arrayData.push({
      id: this.arrayData.length,
      description: '123',
    });
  }

  public level: '1' | '2' | '3' | null = null;

  public randomLevel(): void {
    this.level = String(Math.ceil(Math.random() * 3)) as '1' | '2' | '3';
  }
}
