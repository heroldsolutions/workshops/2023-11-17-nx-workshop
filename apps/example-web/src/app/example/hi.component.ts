import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'example-hi',
  standalone: true,
  imports: [],
  template: `Hi!!!`,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export default class HiComponent {
  constructor() {
    console.log('HiComponent');
  }
}
