import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  standalone: true,
  pure: true,
  name: 'bar',
})
export class BarPipe implements PipeTransform {
  constructor() {
    console.log('BarPipe');
    // throw new Error();
  }

  public transform(value: string) {
    return `${value} bar`;
  }
}
