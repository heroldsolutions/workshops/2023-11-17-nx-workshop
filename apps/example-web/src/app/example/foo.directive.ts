import { Directive } from '@angular/core';

@Directive({
  selector: '[exampleFoo]',
  standalone: true,
})
export class FooDirective {
  constructor() {
    console.log('FooDirective');
  }
}
