import { Route } from '@angular/router';

export const appRoutes: Route[] = [
  {
    path: 'example',
    loadComponent: async () => import('./example/example.component'),
    providers: [],
  },
  {
    path: 'addition',
    loadChildren: async () => import('./example/addition.routes'),
  },
  {
    path: 'clean',
    loadComponent: async () => import('./example/clean.component'),
  },
  {
    path: 'form',
    loadComponent: async () => import('./form/form.component'),
  },
];
