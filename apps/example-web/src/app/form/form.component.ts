import { JsonPipe } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  inject,
  signal,
} from '@angular/core';
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  NonNullableFormBuilder,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';

@Component({
  standalone: true,
  imports: [ReactiveFormsModule, JsonPipe],
  template: `
    Forms
    <input [formControl]="control" />
    <form [formGroup]="form">
      <input type="number" formControlName="input1" />
      {{ form.get('input1')?.valid }}
      {{ form.controls.input1.errors | json }}
      <input formControlName="input2" />
    </form>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export default class FormComponent {
  public control = new FormControl();
  public groups = new FormGroup({
    key1: new FormControl(123),
    key2: new FormControl(),
  });
  public arrays = new FormArray([new FormControl()]);

  public form = inject(FormBuilder).group(
    {
      input1: [
        null as number | null,
        [
          Validators.min(250),
          (input: AbstractControl) => {
            if (typeof input.value === 'string') {
              console.log(input.value);
              return {
                customValidation: 'message in here',
              };
            }
            return null;
          },
        ],
      ],
      input2: '123',
    },
    {
      validators: [
        (group) => {
          const control = group.get('something');
          if (control == null) {
            return {
              isNull: 'Is null',
            };
          }
          const value = control.value;
          if (typeof value === 'string') {
            console.log(value);
          }
          console.log(group.value);
          return null;
        },
      ],
    }
  );

  constructor() {
    this.control.valueChanges;
    this.groups.controls.key1.valueChanges;
    // this.groups.valueChanges.subscribe(console.log);

    this.form.controls.input1.setValue(345);
    this.form.patchValue(
      {
        input1: 123,
      },
      {}
    );
    this.form.setValue({
      input1: 234,
      input2: '2345',
    });
  }

  private readonly fb = inject(FormBuilder);
  public formGroup: FormGroup = this.fb.group({
    groupInGroup: this.fb.group({}),
    control: this.fb.control(123),
    simple: [
      {
        value: 123,
        disabled: true,
      },
    ],
  });

  public initForm(resource: Record<string, string>): void {
    // this.formGroup = this.fb.group({
    //   value1: [resource.value1 ?? null]
    // });
    this.formGroup.setValue(resource, {
      emitEvent: false,
    });
    this.formGroup.markAsPristine();
    this.formGroup.reset();
  }
}
